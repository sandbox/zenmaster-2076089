<?php

/**
 * Implements hook_dbdd_info().
 */
function dbdd_behat_dbdd_info() {
  return array(
    'type' => array('story'),
    'bin' => dbdd_get_composer_path() . 'bin/behat',
    'arguments' => 'dbdd_behat_bin_args',
  );
}

/**
 * Implements hook_dbdd_init().
 * Write the behat.yml config file at the root of the Drupal installation.
 */
function dbdd_behat_dbdd_init() {
  $behat_conf  = 'default:' . PHP_EOL;
  $behat_conf .= '  paths:' . PHP_EOL;
  $behat_conf .= "    features: 'sites/all/modules'" . PHP_EOL;
  $behat_conf .= "    bootstrap: 'sites/all/modules/dbdd/frameworks/story/dbdd_behat/bootstrap'" . PHP_EOL;
  $behat_conf .= '  extensions:' . PHP_EOL;
  $behat_conf .= '    Behat\MinkExtension\Extension:' . PHP_EOL;
  $behat_conf .= '      goutte: ~' . PHP_EOL;
  $behat_conf .= '      selenium2: ~ '. PHP_EOL;
  $behat_conf .= '      base_url: ' . $GLOBALS['base_url'] . PHP_EOL;
  $behat_conf .= '    Drupal\DrupalExtension\Extension:' . PHP_EOL;
  $behat_conf .= '      blackbox: ~' . PHP_EOL;
  $behat_conf .= '      api_driver: "drupal"' . PHP_EOL;
  $behat_conf .= '      drush:' . PHP_EOL;
  $behat_conf .= '        root: ' . DRUPAL_ROOT . PHP_EOL;
  $behat_conf .= '      drupal:' . PHP_EOL;
  $behat_conf .= '        drupal_root: ' . DRUPAL_ROOT . PHP_EOL;
  if (file_exists('dbdd_behat.yml')) {
    $behat_custom_conf .= file_get_contents('dbdd_behat.yml');
    $behat_conf .= $behat_custom_conf;
  }
  file_put_contents('behat.yml', $behat_conf);
}

/**
 * @returns string
 *   Formatted arguments given the scenario parameter.
 */
function dbdd_behat_bin_args($arguments = NULL) {

  $args = array();
  $arguments = explode(' ', $arguments);

  // arguments starting with "-" or "--" will be used by drush itself,
  // so we prefix "--" to the command arguments and use double quotes when multiple:
  foreach ($arguments as $arg) {

    $current_options = drush_get_merged_options();
    $current_drush_alias = $current_options['#name'];

    if (isset($current_drush_alias)) {
      $args[] = '--profile=' . $current_drush_alias;
    }

    $args[] = '--ansi';

    // An active module's name as argument launch its stories.
    $module_path = drupal_get_path('module', $arg);
    if ($module_path) {
      $args[] = $module_path . '/user_stories';
    }
    // else it is a behat argument.
    else {
      $args[] = '--' . $arg;
    }

  }

  return implode(' ', $args);

}

<?php

function dbdd_drush_command() {

  $items = array();
  $items['dbdd-init'] = array(
    'description' => 'Run initialisation steps on activated bdd frameworks',
    'arguments' => array(
      'set' => 'Set the current activated story framework',
      'framework' => 'Set the current activated story framework',
    ),
  );
  $items['dbdd-stories'] = array(
    'description' => 'Test a bdd story',
    'arguments' => array(
      'arguments' => 'Arguments for the story framework\'s command',
    ),
  );
  $items['dbdd-specs'] = array(
    'description' => 'Test bdd specs',
    'arguments' => array(
      'arguments' => 'Arguments for the spec framework\'s command',
    ),
  );

  return $items;

}

/**
 * Performs bdd frameworks initialisation steps.
 * @see dbdd_behat_dbdd_init() (default framework).
 */ 
function drush_dbdd_init($set = NULL, $framework = NULL) {

  if (isset($set) && isset($framework)) {
    if ($set == 'set-story') {
      variable_set('dbdd_story_framework', $framework);
    }
    else if ($set == 'set-spec') {
      variable_set('dbdd_spec_framework', $framework);
    }
    else {
      $error  = 'Invalid arguments: ' . $set . ' ' . $framework . PHP_EOL;
      $error .= 'Correct syntax is: drush dbdd-init set-story|set-spec framework_name';
      echo $error;
    }
  }
  module_invoke(variable_get('dbdd_story_framework', 'dbdd_behat'), 'dbdd_init');
  module_invoke(variable_get('dbdd_spec_framework', 'dbdd_phpunit'), 'dbdd_init');
}

/**
 * Run the story framework's binary and arguments.
 */
function drush_dbdd_stories($args = NULL) {
  // Init step is useful in case there is specific site config, it will
  // be given by drush alias (dbdd_behat_dbdd_init).
  dbdd_get_framework_bin('story', $args);
}

/**
 * Run the specs framework's binary and arguments.
 */
function drush_dbdd_specs($args = NULL) {
  dbdd_get_framework_bin('spec', $args);
}
